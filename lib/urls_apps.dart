import 'package:url_launcher/url_launcher.dart';


String url_youtube = 'https://www.youtube.com/';
String url_whatsapp = 'https://web.whatsapp.com/';
String url_cobranca_in_loco = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUMTJWRFpONU5aQUdZNEdXRVVKSk1HMVo4Si4u';
String url_medidor_divergente = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUQVMySUUxWlQxVFM5RjBMU1M5UkcyQkVCVS4u';
String url_rotoreirizacao = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9c7uTBfU7-JHoEXJ89qvBV5URFAxVE9LMEIwRENOWk5KRTY4V1VBWThXRi4u';
String url_entrega_de_faturas = 'https://docs.google.com/forms/d/e/1FAIpQLSepo93U06MhV1GWTH0Tx0rKxTDxImyfamS32xzAuQHR4wjZqw/viewform';
String url_negociacao = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUMTJWRFpONU5aQUdZNEdXRVVKSk1HMVo4Si4u';
String url_variacao = 'https://forms.office.com/pages/responsepage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUNUZBMkw0TEVXMEM5WDRIUlozSFlSNFkwVC4u';
String url_teams = 'https://www.microsoft.com/pt-br/microsoft-teams/log-in';
String url_meu_rh = 'https://portalmeurh.energisa.com.br/FrameHTML/Web/App/RH/PortalMeuRH/#/login';


void openCobranca(){openUrl(url_cobranca_in_loco);}
void openMedidorDivergente(){openUrl(url_medidor_divergente);}
void openRoteirizacao(){openUrl(url_rotoreirizacao);}
void openEntregaDeFaturas(){openUrl(url_entrega_de_faturas);}
void openNegociacao(){openUrl(url_negociacao);}
void openVaricao(){openUrl(url_variacao);}
void openTeams(){openUrl(url_teams);}
void openMeuRh(){openUrl(url_meu_rh);}
void openYoutube(){openUrl(url_youtube);}
void openWhatsApp(){openUrl(url_whatsapp);}

void openUrl(String url) {

  var parse_url = Uri.parse(url);
  launchUrl(parse_url);

} 