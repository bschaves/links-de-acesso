import 'package:flutter/material.dart';
import 'package:links_de_acesso/urls_apps.dart'; // LIB LOCAL
import 'dart:io';

final url_image = 'https://i.imgur.com/fzgwYzq.png';
//final url_image = 'https://imgur.com/gallery/MyHoi.png';

//var _image = 'fotos/tigre.jpeg';
var _image = 'fotos/chrome.png';
var app_image = _image;
File file_image = new File(app_image);

double default_h = 40;
double defatult_w = 400;



class StackFlutter extends StatelessWidget {

  String title;

  StackFlutter(this.title);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
                    centerTitle: true,
                    title: Text(
                              this.title,
                              selectionColor: Colors.green,
                          ),
                    backgroundColor: Colors.blueAccent,
                    foregroundColor: Colors.white,
                  ),
      body: incluiStack(),
    );
  }
}


Widget incluiStack() {

  return Stack(
        alignment: Alignment.center,
        
        children: <Widget> [
        
                Image.file(
                  file_image,
                  width: double.infinity, 
                  height: double.infinity,
                  //fit: BoxFit.cover,
                  ),

          // COBRANÇA.
          Positioned(
              top: 350.0,
              height: default_h,
              width: defatult_w,
              child:
                  ElevatedButton(
                      onPressed: openCobranca, 
                      child: const Text('Link Cobrança in loco', selectionColor: Colors.green,)
                      ),
            ),

            // ROTEIRIZAÇÃO.
            Positioned(
                top: 300.0,
                height: default_h,
                width: defatult_w,
                child:
                  ElevatedButton(
                      onPressed: openRoteirizacao, 
                      child: const Text('Link Roteirização', selectionColor: Colors.greenAccent,)
                      ),
            ),

            // ENTREGA DE FATURAS.
              Positioned(
                    top: 250.0,
                    height: default_h,
                    width: defatult_w,
                    child:
                  ElevatedButton(
                      onPressed: openEntregaDeFaturas, 
                      child: const Text('Link Entrega de Faturas', selectionColor: Colors.green,)
                      ),
              ),

            // VARIAÇÃO.
              Positioned(
                top: 200.0,
                height: default_h,
                width: defatult_w,
                child:
                  ElevatedButton(
                        onPressed: openVaricao, 
                      child: const Text('Link das variações', selectionColor: Colors.green,)
                      ),
                ),

                // NEGOCIAÇÃO.
                Positioned(
                    top: 150.0,
                    height: default_h,
                    width: defatult_w,
                    child: 
                      ElevatedButton(
                        onPressed: openNegociacao, 
                      child: const Text('Link Negociação', selectionColor: Colors.black,)
                      )
                  ),

                 // TEAMS.
                Positioned(
                    top: 100.0,
                    height: default_h,
                    width: defatult_w,
                    child: 
                      ElevatedButton(
                        onPressed: openTeams, 
                      child: const Text('Teams Web', selectionColor: Colors.black,)
                      )
                  ),


                 // MEU RH.
                Positioned(
                    top: 50.0,
                    height: default_h,
                    width: defatult_w,
                    child: 
                      ElevatedButton(
                        onPressed: openMeuRh, 
                        child: const Text('Meu RH', selectionColor: Colors.black,)
                      )
                  ),



                  // AUTHOR.
                Positioned(
                    top: 410.0,
                    height: 25.0,
                    width: defatult_w,
                    child: 
                        Container(
                          height: 20,
                          color: Colors.grey,
                          child: 
                              Text(
                                  'Autor: Bruno da Silva Chaves', 
                                  textAlign: TextAlign.center,
                                  selectionColor: Colors.black,
                            
                                  )
                        )
                      
                  ),
                  
                  
        ],
  );

}
