import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

final url_image = 'https://i.imgur.com/fzgwYzq.png';
//final url_image = 'https://imgur.com/gallery/MyHoi.png';

//var _image = 'fotos/tigre.jpeg';
var _image = 'fotos/chrome.png';
var app_image = _image;
File file_image = new File(app_image);

String url_youtube = 'https://www.youtube.com/';
String url_whatsapp = 'https://web.whatsapp.com/';
String url_cobranca_in_loco = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUMTJWRFpONU5aQUdZNEdXRVVKSk1HMVo4Si4u';
String url_medidor_divergente = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUQVMySUUxWlQxVFM5RjBMU1M5UkcyQkVCVS4u';
String url_rotoreirizacao = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9c7uTBfU7-JHoEXJ89qvBV5URFAxVE9LMEIwRENOWk5KRTY4V1VBWThXRi4u';
String url_entrega_de_faturas = 'https://docs.google.com/forms/d/e/1FAIpQLSepo93U06MhV1GWTH0Tx0rKxTDxImyfamS32xzAuQHR4wjZqw/viewform';
String url_negociacao = 'https://forms.office.com/Pages/ResponsePage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUMTJWRFpONU5aQUdZNEdXRVVKSk1HMVo4Si4u';
String url_variacao = 'https://forms.office.com/pages/responsepage.aspx?id=hoDG-Fy5y0mdOO03lAq-9V2bJcWMckBOvyiy80KWE5BUNUZBMkw0TEVXMEM5WDRIUlozSFlSNFkwVC4u';
String url_teams = 'https://www.microsoft.com/pt-br/microsoft-teams/log-in';
String url_meu_rh = 'https://portalmeurh.energisa.com.br/FrameHTML/Web/App/RH/PortalMeuRH/#/login';

double default_h = 40;
double defatult_w = 400;

void openCobranca(){openUrl(url_cobranca_in_loco);}
void openMedidorDivergente(){openUrl(url_medidor_divergente);}
void openRoteirizacao(){openUrl(url_rotoreirizacao);}
void openEntregaDeFaturas(){openUrl(url_entrega_de_faturas);}
void openNegociacao(){openUrl(url_negociacao);}
void openVaricao(){openUrl(url_variacao);}
void openTeams(){openUrl(url_teams);}
void openMeuRh(){openUrl(url_meu_rh);}
void openYoutube(){openUrl(url_youtube);}
void openWhatsApp(){openUrl(url_whatsapp);}

void openUrl(String url) {

  var parse_url = Uri.parse(url);
  launchUrl(parse_url);

} 


class StackFlutter extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
                    centerTitle: true,
                    title: Text(
                              'LINKS DE ACESSO V1.2',

                              selectionColor: Colors.green,
                    ),
                    backgroundColor: Colors.blueAccent,
                    foregroundColor: Colors.white,
                  ),
      body: incluiStack(),
    );
  }
}


Widget incluiStack() {

  return Stack(
        alignment: Alignment.center,
        
        children: <Widget> [
        
                Image.file(
                  file_image,
                  width: double.infinity, 
                  height: double.infinity,
                  //fit: BoxFit.cover,
                  ),

          // COBRANÇA.
          Positioned(
              top: 350.0,
              height: default_h,
              width: defatult_w,
              child:
                  ElevatedButton(
                      onPressed: openCobranca, 
                      child: const Text('Link Cobrança in loco', selectionColor: Colors.green,)
                      ),
            ),

            // ROTEIRIZAÇÃO.
            Positioned(
                top: 300.0,
                height: default_h,
                width: defatult_w,
                child:
                  ElevatedButton(
                      onPressed: openRoteirizacao, 
                      child: const Text('Link Roteirização', selectionColor: Colors.greenAccent,)
                      ),
            ),

            // ENTREGA DE FATURAS.
              Positioned(
                    top: 250.0,
                    height: default_h,
                    width: defatult_w,
                    child:
                  ElevatedButton(
                      onPressed: openEntregaDeFaturas, 
                      child: const Text('Link Entrega de Faturas', selectionColor: Colors.green,)
                      ),
              ),

            // VARIAÇÃO.
              Positioned(
                top: 200.0,
                height: default_h,
                width: defatult_w,
                child:
                  ElevatedButton(
                        onPressed: openVaricao, 
                      child: const Text('Link das variações', selectionColor: Colors.green,)
                      ),
                ),

                // NEGOCIAÇÃO.
                Positioned(
                    top: 150.0,
                    height: default_h,
                    width: defatult_w,
                    child: 
                      ElevatedButton(
                        onPressed: openNegociacao, 
                      child: const Text('Link Negociação', selectionColor: Colors.black,)
                      )
                  ),

                 // TEAMS.
                Positioned(
                    top: 100.0,
                    height: default_h,
                    width: defatult_w,
                    child: 
                      ElevatedButton(
                        onPressed: openTeams, 
                      child: const Text('Teams Web', selectionColor: Colors.black,)
                      )
                  ),


                 // MEU RH.
                Positioned(
                    top: 50.0,
                    height: default_h,
                    width: defatult_w,
                    child: 
                      ElevatedButton(
                        onPressed: openMeuRh, 
                        child: const Text('Meu RH', selectionColor: Colors.black,)
                      )
                  ),



                  // AUTHOR.
                Positioned(
                    top: 410.0,
                    height: 25.0,
                    width: defatult_w,
                    child: 
                        Container(
                          height: 20,
                          color: Colors.grey,
                          child: 
                              Text(
                                  'Autor: Bruno da Silva Chaves', 
                                  textAlign: TextAlign.center,
                                  selectionColor: Colors.black,
                            
                                  )
                        )
                      
                  ),
                  
                  
        ],
  );

}
